/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package neuralnetwork;

import java.util.*;


/**
 *
 * @author Oliver
 */
public class Neuron{
    
    private ArrayList<Neuron> inputs = new ArrayList();
    private float weight;
    private float threshold;
    private boolean fired = false;
    
    public Neuron(float threshold){
        this.threshold = threshold;
    }
    
    public void connect(Neuron... neurons){
        inputs.addAll(Arrays.asList(neurons));
    }
    
    public void setWeight(float weight){
        this.weight = weight;
    }
    
    public void setWeight(boolean weight){
        this.weight = weight ? 1.0f : 0.0f;
    }
    
    public float getWeight(){
        return weight;
    }
    
    public float fire(){
        if(inputs.size() > 0){
            float totalweight = 0.0f;
            for(Neuron n : inputs){
                n.fire();
                totalweight += (n.isFired()) ? n.getWeight() : 0.0f;
            }
            fired = totalweight > threshold;
            return totalweight;
        }else if(weight != 0.0f){
            fired = weight > threshold;
            return weight;
        }
        return 0.0f;
    }
    
    public boolean isFired(){
        return fired;
    }
}
